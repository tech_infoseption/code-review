//GrabResolver and Grapes are used to manage JAR dependency resolution for Maven Central Repository
@GrabResolver(name='repo.jenkins-ci.org', root='http://repo.jenkins-ci/public/')
//Kohsuke represents GitHub API in Java based OOPS way.
@Grapes([@Grab(group='org.kohsuke', module='github-api', version='1.59')])

import static hudson.security.ACL.SYSTEM
import com.cloudbees.plugins.credentials.CredentialsProvider
import com.cloudbees.plugins.credentials.common.StandardCredentials
import hudson.Plugin 
import hudson.util.VersionNumber
import jenkins.model.Jenkins
import org.kohsuke.github.Github 
import org.kohsuke.github.GHOrganization

//This information can be obtained from Manage Jenkins -> Configure System
GITHUB_ORGANIZATION_NAME = 'ORGANIZATION_NAME'
CREDENTIAL_PLUGIN_GITHUB_API_KEY_ID = 'CREDENTIAL_ID_FOR EVERY_JOB'

def getCredentials(credentialsId){
    Jenkins jenkins = Jenkins.getInstance();
    Plugin credentialsPlugin = jenkins.getPlugin('credentials')
    if(credentialsPlugin != null && !credentialsPlugin.getWrapper().getVersionNumber().isOlderThan(new VersionNumber('1.6'))){
        for(CredentialsProvider credentialsProvider : jenkins.getExtensionList(CredentialsProvider.class)){
            for(StandardCredentials credentials : credentialsProvider.getCredentials(StandardCredentials.class, jenkins, SYSTEM)){
                if(credentials.getDescription().equals(credentialsId) || credentials.getId().equals(credentialsId)){
                    return credentials.getSecret().toString();
                }
            }
        }
        throw new IllegalArgumentException('Unable to find credential with ID: ' +credentialsId)
    }
}

//Retrieving Repo Information from Github using Kohsuke's OOPS based GitHub API
def getRepos(credentials, organizationName){
    Github github = Github.connectUsingOAuth(credentials)
    GHOrganization org = github.getOrganization(organizationName)
    return org.getRepositories()
}

for(repo in REPOS){
    repoDetails = repo.getValue()
    repoName = repoDetails.getName()
    repoSshUrl = repoDetails.getSshUrl()

    //Job Creation
    job(type: Maven){
        name ("${repoName}")
        triggers {scm(0 0 23 1/1 * ? *)}
        scm{
            git{
                remote{
                    url("https://github.com/ackris/code-review")
                }
                createTag(false)
            }
        }

        rootPOM("${repoName}/pom.xml")
        goals("clean package")
        wrappers{
            preBuildCleanup()
            release{
                preBuildSteps{
                    maven{
                        mavenInstallation("Maven 3.3.9")
                        goals("build-helper:parse-version")
                        goals("version:set")
                        property("newVersion", "\${parsedVersion.majorVersion}.\${parsedVersion.minorVersion}.\${parsedVersion.incrementalVersion}-\${BUILD_NUMBER}")
                    }
                }
                postSuccessfulBuildSteps{
                    maven{
                        goals("deploy")
                    }
                    maven{
                        goals("scm:tag")
                    }
                    downstreamParameterized{
                        trigger("deploy-application"){
                            predefinedProp("STAGE", "development")
                        }
                    }
                }
            }
        }
        publishers{
            groovyPostBuild("manager.addShortText(manager.build.getEnvironment(manager.listener)[\'POM_VERSION\'])")
        }
        promotions{
            promotion("Development"){
                icon("star-red")
                conditions{
                    manual('')
                }
                actions{
                    downstreamParameterized{
                        trigger("deploy-application", "SUCCESS", false, ["buildStepFailure": "FAILURE", "failure": "FAILURE", "unstable": "UNSTABLE"]){
                            predefinedProp("ENVIRONMENT", "test-server")
                            predefinedProp("APPLICATION_NAME", "\${PROMOTED_FULL_JOB_NAME}")
                            predefinedProp("BUILD_ID", "\${PROMOTED_NUMBER}")
                        }
                    }
                }
            }
            promotion("QA"){
                icon("star-yellow")
                conditions{
                    manual('')
                    upstream("Development")
                }
                actions{
                    downstreamParameterized{
                        trigger("deploy-application", "SUCCESS", false, ["buildStepFailure": "FAILURE", "failure": "FAILURE", "unstable": "UNSTABLE"]){
                            predefinedProp("ENVIRONMENT", "qa-server")
                            predefinedProp("APPLICATION_NAME", "\${PROMOTED_FULL_JOB_NAME}")
                            predefinedProp("BUILD_ID", "\${PROMOTED_NUMBER}")
                    }
                }
            }
        }
        promotion("Production"){
            icon("star-green")
            conditions{
                manual('')
                upstream("QA")
            }
            actions{
                downstreamParameterized{
                     trigger("deploy-application", "SUCCESS", false, ["buildStepFailure": "FAILURE", "failure": "FAILURE", "unstable": "UNSTABLE"]){
                            predefinedProp("ENVIRONMENT", "prod-server")
                            predefinedProp("APPLICATION_NAME", "\${PROMOTED_FULL_JOB_NAME}")
                            predefinedProp("BUILD_ID", "\${PROMOTED_NUMBER}")
                }
            }
        }
    }
}


